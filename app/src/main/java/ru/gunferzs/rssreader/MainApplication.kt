package ru.gunferzs.rssreader

import android.app.Application
import android.content.Context
import ru.gunferzs.rssreader.data.db.PreferenceHelper
import ru.gunferzs.rssreader.data.db.helpers.DBHelper
import ru.gunferzs.rssreader.data.server.ConnectionStatus
import ru.gunferzs.rssreader.domain.executors.impl.MainThread

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ConnectionStatus.setInstance(this)
        PreferenceHelper.setInstance(this)
        DBHelper.setInstance(this)
    }
}