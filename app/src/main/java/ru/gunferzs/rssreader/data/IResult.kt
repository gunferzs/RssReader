package ru.gunferzs.rssreader.data

interface IResult<Model, Error> {
    fun onSuccess(model: Model?)
    fun onError(error: Error?)
    fun onComplete()
}