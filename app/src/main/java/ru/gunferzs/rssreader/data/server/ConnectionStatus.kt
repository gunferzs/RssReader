package ru.gunferzs.rssreader.data.server

import android.content.Context
import android.net.ConnectivityManager


class ConnectionStatus(context: Context) {

    var connectivityManager: ConnectivityManager

    init {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    companion object {
        const val DB_NAME = "RssFeedDataBase"

        @Volatile
        public var INSTANCE: ConnectionStatus? = null

        fun setInstance(context: Context): ConnectionStatus {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: ConnectionStatus(context).also { INSTANCE = it }
            }
        }
    }

    fun isConnected(): Boolean {
        val activeNetwork = connectivityManager.getActiveNetworkInfo()
        return activeNetwork?.isConnectedOrConnecting() ?: false
    }
}