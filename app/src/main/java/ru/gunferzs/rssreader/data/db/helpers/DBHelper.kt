package ru.gunferzs.rssreader.data.db.helpers

import android.content.Context
import android.content.res.AssetManager
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import org.simpleframework.xml.core.Persister
import ru.gunferzs.rssreader.data.db.RssFeedInsertion
import ru.gunferzs.rssreader.data.db.RssNewsInsertion
import ru.gunferzs.rssreader.data.model.Rss
import java.io.IOException

class DBHelper(context: Context?) : SQLiteOpenHelper(context, DB_NAME, null, 1) {

    var assetManager: AssetManager?

    init {
        assetManager = context?.assets
    }

    companion object {
        const val DB_NAME = "RssFeedDataBase"

        @Volatile
        public var INSTANCE: DBHelper? = null

        fun setInstance(context: Context): DBHelper {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: DBHelper(context).also { INSTANCE = it }
            }
        }

    }

    override fun onCreate(db: SQLiteDatabase?) {
        val firstInitXml = getXml("news.xml")
        db?.execSQL(RssFeedTableInfoForExecution().createTableSql())
        db?.execSQL(RssNewsTableInfoForExecution().createTableSql())
        val serializer = Persister()
        val rss = serializer.read(Rss::class.java, firstInitXml)
        val id = RssFeedInsertion().insert(rss, db, "https://www.audit-it.ru/rss/news_account.xml")
        RssNewsInsertion().insert(rss.channel.items, db, id)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    private fun getXml(fileName: String): String? {
        var xmlString: String? = null

        try {
            val stream = assetManager?.open(fileName)
            val length = stream?.available() ?: 0
            val data = ByteArray(length)
            stream?.read(data)
            xmlString = String(data)
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return xmlString
    }

}