package ru.gunferzs.rssreader.data.repository

import ru.gunferzs.rssreader.data.IResult
import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.data.repository.base.IRepository

interface IRssRepository : IRepository {

    fun downloadRssNews(idRssFeed: Int)

    fun downloadRssNewsWithSavedId()

    fun getListChannels()


    interface RssResult : IResult<Rss, String?>

    interface ChannelsResult : IResult<List<Rss>, String?>
}