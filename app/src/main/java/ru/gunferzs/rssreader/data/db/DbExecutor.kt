package ru.gunferzs.rssreader.data.db

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import ru.gunferzs.rssreader.data.db.helpers.DBHelper

class DbExecutor : IDbExecutor {

    var db: SQLiteDatabase?

    init {
        db = DBHelper.INSTANCE?.writableDatabase
    }

    override fun query(select: String): Cursor? {
        return db?.rawQuery(select, null)
    }

    override fun delete(delete: String) {
        db?.execSQL(delete)
    }

    override fun update(update: String) {
        db?.execSQL(update)
    }

    override fun insert(tableName: String, contentValues: ContentValues): Long? {
        return db?.insert(tableName, null, contentValues)
    }

}