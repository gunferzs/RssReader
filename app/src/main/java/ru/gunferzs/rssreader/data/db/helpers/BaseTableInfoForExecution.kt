package ru.gunferzs.rssreader.data.db.helpers

import android.content.ContentValues

abstract class BaseTableInfoForExecution : ITableInfoForExecution {

    override fun updateRowsSql(fields: Array<String>, values: Array<String>, conditions: Array<String>, valuesCondition: Array<String>, fieldsCondition: Array<String>): String {
        var update = "update ${getTableName()} set "

        for (i in fields.indices) {
            update += "${fields[i]} = ${values[i]}"

            if (i < fields.size - 1) {
                update += ", "
            }
        }

        update += "where "

        for (i in fieldsCondition.indices) {
            update += "${fieldsCondition[i]} ${conditions[i]} ${valuesCondition[i]}"

            if (i < fields.size - 1) {
                update += ", "
            }
        }

        return update
    }

    override fun deleteRowsSql(fields: Array<String>, values: Array<String>, conditions: Array<String>): String {
        var delete = "delete from ${getTableName()} where "

        for (i in fields.indices) {
            delete += "${fields[i]} ${conditions[i]} ${values[i]}"

            if (i < fields.size - 1) {
                delete += ", "
            }
        }

        return delete
    }

    override fun selectRowsSql(condition: String?): String {
        var select = "select * from ${getTableName()}"

        condition?.let {
            select += " where ${condition}"
        }

        return select
    }

    override fun insertRowSql(columns: Array<String>, values: Array<String>): ContentValues {
        val insert = ContentValues()

        for (i in columns.indices) {
            insert.put(columns[i], values[i])
        }

        return insert
    }

}