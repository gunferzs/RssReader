package ru.gunferzs.rssreader.data.db.helpers

class RssFeedTableInfoForExecution : BaseTableInfoForExecution() {

    companion object {
        const val URL: String = "url"
        const val TABLE: String = "rss_feed"
        const val ID: String = "id"
        const val LANGUAGE: String = "language"
        const val TITLE: String = "title"
        const val DESCRIPTION: String = "description"
        const val LINK: String = "link"
        const val IMAGE_URL: String = "image_url"
    }

    override fun getTableName(): String {
        return TABLE
    }

    override fun createTableSql(): String {
        return "create table " + getTableName() + " (" +
                ID + " integer primary key autoincrement," +
                LANGUAGE + " text," +
                TITLE + " text," +
                DESCRIPTION + " text," +
                LINK + " text," +
                URL + " text," +
                IMAGE_URL + " text)";
    }


}