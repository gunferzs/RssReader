package ru.gunferzs.rssreader.data.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.util.Collections;
import java.util.List;

@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2005/Atom", prefix = "atom")
})
@Root(name = "channel", strict = false)
public class Channel {
    @Element(name = "language", required = false)
    String language;
    @Element(name = "title", required = false)
    String title;
    @Element(name = "description", required = false)
    String description;
    @ElementList(entry = "link", inline = true, required = false)
    private List<String> links;
    @Element(name = "image", required = false)
    Image image;
    @ElementList(inline = true)
    List<Item> items;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<String> getLinks() {
        return Collections.unmodifiableList(links);
    }

    public void setLinks(final List<String> links) {
        this.links = links;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(final Image image) {
        this.image = image;
    }

    public List<Item> getItems() {
        return Collections.unmodifiableList(items);
    }

    public void setItems(final List<Item> items) {
        this.items = items;
    }
}
