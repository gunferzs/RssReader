package ru.gunferzs.rssreader.data.repository.impl

import ru.gunferzs.rssreader.data.db.DbExecutor
import ru.gunferzs.rssreader.data.db.RssFeedInsertion
import ru.gunferzs.rssreader.data.db.RssNewsInsertion
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution
import ru.gunferzs.rssreader.data.repository.IRssFeedRepository
import ru.gunferzs.rssreader.data.server.ConnectionStatus
import ru.gunferzs.rssreader.data.server.ServerApi

class RssFeedRepository(val callback: IRssFeedRepository.RssFeedResult) : IRssFeedRepository {

    var db: DbExecutor

    init {
        db = DbExecutor()
    }

    override fun addLink(url: String) {
        val feed = db.query(RssFeedTableInfoForExecution().selectRowsSql("'${RssFeedTableInfoForExecution.URL}=${url}'"))

        feed?.moveToFirst()

        if (feed?.count ?: -1 > 0) {
            callback.onError("Is exist")
        } else {
            if (ConnectionStatus.INSTANCE?.isConnected() ?: false) {
                val response = ServerApi.instance.getRss(url)

                if (response.isSuccessful) {
                    val id = RssFeedInsertion().insert(response.body()!!, db, url)
                    RssNewsInsertion().insert(response.body()?.channel?.items.orEmpty(), db, id?.toLong())
                    callback.onSuccess(true)
                } else {
                    var error: String = "Error (" + response.code() + ") "

                    response.errorBody()?.let {
                        error += it.string()
                    }

                    callback.onError(error)
                }
            } else {
                callback.onError("No connection")
            }
        }

        callback.onComplete()
    }
}