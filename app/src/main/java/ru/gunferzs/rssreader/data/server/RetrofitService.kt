package ru.gunferzs.rssreader.data.server

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url
import ru.gunferzs.rssreader.data.model.Rss

interface RetrofitService {

    @GET()
    fun getRss(@Url url: String): Call<Rss>
}