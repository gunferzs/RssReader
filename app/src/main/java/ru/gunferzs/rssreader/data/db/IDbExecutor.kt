package ru.gunferzs.rssreader.data.db

import android.content.ContentValues
import android.database.Cursor

interface IDbExecutor {
    fun query(select: String): Cursor?
    fun delete(delete: String)
    fun update(update: String)
    fun insert(tableName: String, contentValues: ContentValues): Long?
}