package ru.gunferzs.rssreader.data.db.helpers

import android.content.ContentValues

interface ITableInfoForExecution {
    fun getTableName(): String
    fun createTableSql(): String
    fun updateRowsSql(fields: Array<String>, values: Array<String>, conditions: Array<String>, valuesCondition: Array<String>, fieldsCondition: Array<String>): String
    fun deleteRowsSql(fields: Array<String>, values: Array<String>, conditions: Array<String>): String
    fun selectRowsSql(condition: String?): String
    fun insertRowSql(columns: Array<String>, values: Array<String>): ContentValues
}