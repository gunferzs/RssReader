package ru.gunferzs.rssreader.data.repository.impl

import ru.gunferzs.rssreader.data.db.DbExecutor
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution
import ru.gunferzs.rssreader.data.model.Item
import ru.gunferzs.rssreader.data.repository.IRssNewsItemRepository

class RssNewsItemRepository(val callback: IRssNewsItemRepository.RssNewsResult) : IRssNewsItemRepository {

    var db: DbExecutor

    init {
        db = DbExecutor()
    }

    override fun getNewsItem(id: Int) {
        val news = db.query(RssNewsTableInfoForExecution().selectRowsSql("${RssNewsTableInfoForExecution.ID}=${id}"))
        news?.moveToFirst()
        callback.onSuccess(Item.fromCursor(news))
        callback.onComplete()
    }
}