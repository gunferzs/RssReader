package ru.gunferzs.rssreader.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

@Element(name = "enclosure", required = false)
public class Enclosure {
    @Attribute(name = "url", required = false)
    String url;
    @Attribute(name = "length", required = false)
    private String length;
    @Attribute(name = "type", required = false)
    private String type;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getLength() {
        return length;
    }

    public void setLength(final String length) {
        this.length = length;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
