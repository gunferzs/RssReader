package ru.gunferzs.rssreader.data.db

import android.database.sqlite.SQLiteDatabase
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.CATEGORY
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.DESCRIPTION
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.GUID
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.ID_RSS_FEED
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.IMAGE_URL
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.LINK
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.PUB_DATE
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution.Companion.TITLE
import ru.gunferzs.rssreader.data.model.Item

class RssNewsInsertion {


    fun insert(items: List<Item>, db: DbExecutor?, idRssFeed: Long?) {
        for (item in items) {
            val rss = RssNewsTableInfoForExecution()
            db?.insert(rss.getTableName(), RssNewsTableInfoForExecution().insertRowSql(
                    arrayOf(TITLE, ID_RSS_FEED, GUID, LINK, DESCRIPTION, PUB_DATE, IMAGE_URL, CATEGORY),
                    arrayOf(item.title.orEmpty(), idRssFeed.toString(), item.guid.orEmpty(), item.link.orEmpty(), item.description.orEmpty(), item.pubDate.orEmpty(), item.enclosure?.url.orEmpty(), item.category.orEmpty())))
        }
    }

    fun insert(items: List<Item>, db: SQLiteDatabase?, idRssFeed: Long?) {
        for (item in items) {
            val rss = RssNewsTableInfoForExecution()
            db?.insert(rss.getTableName(), null, RssNewsTableInfoForExecution().insertRowSql(
                    arrayOf(TITLE, ID_RSS_FEED, GUID, LINK, DESCRIPTION, PUB_DATE, IMAGE_URL, CATEGORY),
                    arrayOf(item.title.orEmpty(), idRssFeed.toString(), item.guid.orEmpty(), item.link.orEmpty(), item.description.orEmpty(), item.pubDate.orEmpty(), item.enclosure?.url.orEmpty(), item.category.orEmpty())))
        }
    }
}