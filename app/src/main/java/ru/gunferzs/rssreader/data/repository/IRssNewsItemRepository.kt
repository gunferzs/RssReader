package ru.gunferzs.rssreader.data.repository

import ru.gunferzs.rssreader.data.IResult
import ru.gunferzs.rssreader.data.model.Item
import ru.gunferzs.rssreader.data.repository.base.IRepository

interface IRssNewsItemRepository : IRepository {
    fun getNewsItem(id: Int)

    interface RssNewsResult : IResult<Item, String>
}