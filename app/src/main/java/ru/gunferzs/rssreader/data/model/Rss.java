package ru.gunferzs.rssreader.data.model;

import android.database.Cursor;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution;

@Root(name = "rss", strict = false)
public class Rss {

    private boolean isSelected = false;

    private int idRssFeed;

    private String url;

    @Element(name = "channel")
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(final Channel channel) {
        this.channel = channel;
    }

    public int getIdRssFeed() {
        return idRssFeed;
    }

    public void setIdRssFeed(final int idRssFeed) {
        this.idRssFeed = idRssFeed;
    }

    public String getUrl() {
        return url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public static Rss fromCursor(final Cursor channel, final Cursor news) {
        channel.moveToFirst();
        final Rss rss = new Rss();
        rss.idRssFeed = channel.getInt(channel.getColumnIndex(RssFeedTableInfoForExecution.ID));
        final Channel channelTemp = new Channel();
        channelTemp.description = channel.getString(channel.getColumnIndex(RssFeedTableInfoForExecution.DESCRIPTION));
        channelTemp.title = channel.getString(channel.getColumnIndex(RssFeedTableInfoForExecution.TITLE));
        final Image image = new Image();
        image.url = channel.getString(channel.getColumnIndex(RssFeedTableInfoForExecution.IMAGE_URL));
        channelTemp.image = image;
        channelTemp.language = channel.getString(channel.getColumnIndex(RssFeedTableInfoForExecution.LANGUAGE));
        news.moveToFirst();
        final List<Item> list = new ArrayList<>();

        do {
            list.add(Item.fromCursor(news));
        } while (news.moveToNext());

        channelTemp.items = list;
        rss.channel = channelTemp;

        return rss;
    }

    public static List<Rss> fromCursorOnlyChannelsInfo(final Cursor channels) {
        final List<Rss> list = new ArrayList<>();
        channels.moveToFirst();

        do {
            final Rss rss = new Rss();
            rss.idRssFeed = channels.getInt(channels.getColumnIndex(RssFeedTableInfoForExecution.ID));
            final Channel channelTemp = new Channel();
            channelTemp.description = channels.getString(channels.getColumnIndex(RssFeedTableInfoForExecution.DESCRIPTION));
            channelTemp.title = channels.getString(channels.getColumnIndex(RssFeedTableInfoForExecution.TITLE));
            final Image image = new Image();
            image.url = channels.getString(channels.getColumnIndex(RssFeedTableInfoForExecution.IMAGE_URL));
            channelTemp.image = image;
            channelTemp.language = channels.getString(channels.getColumnIndex(RssFeedTableInfoForExecution.LANGUAGE));
            rss.channel = channelTemp;
            list.add(rss);
        } while (channels.moveToNext());

        return list;
    }

    public static List<Rss> chackedListItem(final List<Rss> list, final int id) {
        for (final Rss rss : list) {
            if (rss.idRssFeed == id) {
                rss.isSelected = true;

                break;
            }
        }

        return list;
    }
}
