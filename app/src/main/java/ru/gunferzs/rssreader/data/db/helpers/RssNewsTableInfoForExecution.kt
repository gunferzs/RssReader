package ru.gunferzs.rssreader.data.db.helpers

class RssNewsTableInfoForExecution : BaseTableInfoForExecution() {

    companion object {
        const val TABLE: String = "rss_news"
        const val ID: String = "id"
        const val ID_RSS_FEED: String = "id_rss_feed"
        const val TITLE: String = "title"
        const val GUID: String = "guid"
        const val LINK: String = "link"
        const val DESCRIPTION: String = "description"
        const val PUB_DATE: String = "pub_date"
        const val IMAGE_URL: String = "image_url"
        const val CATEGORY: String = "category"
    }

    override fun getTableName(): String {
        return TABLE
    }

    override fun createTableSql(): String {
        return "create table " + getTableName() + " (" +
                ID + " integer primary key autoincrement," +
                ID_RSS_FEED + " integer," +
                TITLE + " text," +
                GUID + " text," +
                LINK + " text," +
                DESCRIPTION + " text," +
                PUB_DATE + " text," +
                IMAGE_URL + " text," +
                CATEGORY + " text)"

    }


}