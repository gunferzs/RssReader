package ru.gunferzs.rssreader.data.repository.impl

import ru.gunferzs.rssreader.data.db.DbExecutor
import ru.gunferzs.rssreader.data.db.PreferenceHelper
import ru.gunferzs.rssreader.data.db.RssNewsInsertion
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution
import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution
import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.data.repository.IRssRepository
import ru.gunferzs.rssreader.data.server.ConnectionStatus
import ru.gunferzs.rssreader.data.server.ServerApi

class RssRepository() : IRssRepository {

    lateinit var callbackRss: IRssRepository.RssResult

    lateinit var callbackListChannels: IRssRepository.ChannelsResult

    var db: DbExecutor

    init {
        db = DbExecutor()
    }

    override fun downloadRssNews(idRssFeed: Int) {

        PreferenceHelper.INSTANCE?.setIdRssFeed(idRssFeed)

        if (ConnectionStatus.INSTANCE?.isConnected() ?: false) {
            val rss = db.query(RssFeedTableInfoForExecution().selectRowsSql("${RssFeedTableInfoForExecution.ID}=${idRssFeed}"))
            rss?.moveToFirst()
            val url = rss?.getString(rss.getColumnIndex(RssFeedTableInfoForExecution.URL)) ?: ""
            rss?.close()
            val response = ServerApi.instance.getRss(url)

            if (response.isSuccessful) {
                db.delete(RssNewsTableInfoForExecution().deleteRowsSql(arrayOf(RssNewsTableInfoForExecution.ID_RSS_FEED),
                        arrayOf(idRssFeed.toString()), arrayOf("=")))
                RssNewsInsertion().insert(response.body()?.channel?.items.orEmpty(), db, idRssFeed?.toLong())
            } else {
                var error: String = "Error (" + response.code() + ") "

                response.errorBody()?.let {
                    error += it.string()
                }

                callbackRss.onError(error)
            }
        } else {
            callbackRss.onError("No connection")
        }

        val feed = db.query(RssFeedTableInfoForExecution().selectRowsSql("${RssFeedTableInfoForExecution.ID}=${idRssFeed}"))
        val news = db.query(RssNewsTableInfoForExecution().selectRowsSql("${RssNewsTableInfoForExecution.ID_RSS_FEED}=${idRssFeed}"))

        val rss = Rss.fromCursor(feed, news)
        feed?.close()
        news?.close()
        callbackRss.onSuccess(rss)
        callbackRss.onComplete()
    }

    override fun downloadRssNewsWithSavedId() {

        val idRssFeed = PreferenceHelper.INSTANCE?.getIdRssFeed()

        if (ConnectionStatus.INSTANCE?.isConnected() ?: false) {
            val rss = db.query(RssFeedTableInfoForExecution().selectRowsSql("${RssFeedTableInfoForExecution.ID}=${idRssFeed}"))
            rss?.moveToFirst()
            val url = rss?.getString(rss.getColumnIndex(RssFeedTableInfoForExecution.URL)) ?: ""
            rss?.close()
            val response = ServerApi.instance.getRss(url)

            if (response.isSuccessful) {
                db.delete(RssNewsTableInfoForExecution().deleteRowsSql(arrayOf(RssNewsTableInfoForExecution.ID_RSS_FEED),
                        arrayOf(idRssFeed.toString()), arrayOf("=")))
                RssNewsInsertion().insert(response.body()?.channel?.items.orEmpty(), db, idRssFeed?.toLong())
            } else {
                var error: String = "Error (" + response.code() + ") "

                response.errorBody()?.let {
                    error += it.string()
                }

                callbackRss.onError(error)
            }


        } else {
            callbackRss.onError("No connection")
        }
        val feed = db.query(RssFeedTableInfoForExecution().selectRowsSql("${RssFeedTableInfoForExecution.ID}=${idRssFeed}"))
        val news = db.query(RssNewsTableInfoForExecution().selectRowsSql("${RssNewsTableInfoForExecution.ID_RSS_FEED}=${idRssFeed}"))

        val rss = Rss.fromCursor(feed, news)
        feed?.close()
        news?.close()
        callbackRss.onSuccess(rss)
        callbackRss.onComplete()
    }

    override fun getListChannels() {
        val idRssFeed = PreferenceHelper.INSTANCE?.getIdRssFeed() ?: -1
        val feed = db.query(RssFeedTableInfoForExecution().selectRowsSql(null))
        val list = Rss.fromCursorOnlyChannelsInfo(feed)

        callbackListChannels.onSuccess(Rss.chackedListItem(list, idRssFeed))
        callbackListChannels.onComplete()
    }

}