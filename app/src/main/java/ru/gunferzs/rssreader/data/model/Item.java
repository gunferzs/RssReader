package ru.gunferzs.rssreader.data.model;

import android.database.Cursor;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import ru.gunferzs.rssreader.data.db.helpers.RssNewsTableInfoForExecution;

@Root(name = "item", strict = false)
public class Item {

    private int id;
    @Element(name = "title")
    private String title;
    @Element(name = "guid")
    private String guid;
    @Element(name = "link")
    private String link;
    @Element(name = "description", data = true)
    private String description;
    @Element(name = "pubDate")
    private String pubDate;
    @Element(name = "enclosure", required = false)
    private Enclosure enclosure;
    @Element(name = "category", required = false)
    private String category;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(final String guid) {
        this.guid = guid;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(final String pubDate) {
        this.pubDate = pubDate;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(final Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public static Item fromCursor(final Cursor item) {
        final Item currentItem = new Item();
        currentItem.id = item.getInt(item.getColumnIndex(RssNewsTableInfoForExecution.ID));
        currentItem.title = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.TITLE));
        currentItem.description = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.DESCRIPTION));
        currentItem.guid = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.GUID));
        currentItem.link = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.LINK));
        currentItem.pubDate = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.PUB_DATE));
        currentItem.category = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.CATEGORY));
        final Enclosure enclosure = new Enclosure();
        enclosure.url = item.getString(item.getColumnIndex(RssNewsTableInfoForExecution.IMAGE_URL));
        currentItem.enclosure = enclosure;

        return currentItem;
    }
}
