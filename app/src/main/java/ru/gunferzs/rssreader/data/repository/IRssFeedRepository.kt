package ru.gunferzs.rssreader.data.repository

import ru.gunferzs.rssreader.data.IResult

interface IRssFeedRepository {

    fun addLink(url: String)

    interface RssFeedResult : IResult<Boolean, String>
}