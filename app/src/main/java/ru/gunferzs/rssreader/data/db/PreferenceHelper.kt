package ru.gunferzs.rssreader.data.db

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences


class PreferenceHelper(context: Context) {

    companion object {
        const val PREFERENCE_NAME = "preference_data"
        const val ID_RSS_FEED = "id_rss_feed"

        @Volatile
        var INSTANCE: PreferenceHelper? = null

        fun setInstance(context: Context): PreferenceHelper {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: PreferenceHelper(context).also { INSTANCE = it }
            }
        }
    }

    var preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE)
    }

    fun getIdRssFeed(): Int {
        return preferences.getInt(ID_RSS_FEED, 1)
    }

    fun setIdRssFeed(id: Int) {
        val ed = preferences.edit()
        ed.putInt(ID_RSS_FEED, id)
        ed.apply()
    }

}