package ru.gunferzs.rssreader.data.db

import android.database.sqlite.SQLiteDatabase
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.DESCRIPTION
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.IMAGE_URL
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.LANGUAGE
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.LINK
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.TITLE
import ru.gunferzs.rssreader.data.db.helpers.RssFeedTableInfoForExecution.Companion.URL
import ru.gunferzs.rssreader.data.model.Rss

class RssFeedInsertion {
    fun insert(item: Rss, db: DbExecutor?, url: String): Long? {
        val rss = RssFeedTableInfoForExecution()
        val channel = item.channel

        return db?.insert(rss.getTableName(), RssFeedTableInfoForExecution().insertRowSql(
                arrayOf(TITLE, LANGUAGE, LINK, DESCRIPTION, URL, IMAGE_URL),
                arrayOf(channel.title.orEmpty(), channel.language.orEmpty(), channel.links[0].orEmpty(), channel.description.orEmpty(), url.orEmpty(), channel.image?.url.orEmpty())))
    }

    fun insert(item: Rss, db: SQLiteDatabase?, url: String): Long? {
        val rss = RssFeedTableInfoForExecution()
        val channel = item.channel
        val cv = RssFeedTableInfoForExecution().insertRowSql(
                arrayOf(TITLE, LANGUAGE, LINK, DESCRIPTION, URL, IMAGE_URL),
                arrayOf(channel.title.orEmpty(), channel.language.orEmpty(), channel.links[0], channel.description.orEmpty(), url.orEmpty(), channel.image?.url.orEmpty()))

        return db?.insert(rss.getTableName(), null, cv)
    }
}