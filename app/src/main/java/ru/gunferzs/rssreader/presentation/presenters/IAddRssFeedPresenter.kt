package ru.gunferzs.rssreader.presentation.presenters

import ru.gunferzs.rssreader.presentation.presenters.base.IPresenter
import ru.gunferzs.rssreader.presentation.ui.base.IProgressView
import ru.gunferzs.rssreader.presentation.ui.base.IView

interface IAddRssFeedPresenter : IPresenter {

    fun update(link: String)

    interface IAddRssFeedView : IView, IProgressView {
        fun showToastError(text: String)
        fun back()
    }
}