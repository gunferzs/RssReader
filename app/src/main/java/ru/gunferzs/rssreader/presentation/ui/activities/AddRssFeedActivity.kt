package ru.gunferzs.rssreader.presentation.ui.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_rss_feed.*
import kotlinx.android.synthetic.main.main.*
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.presentation.presenters.IAddRssFeedPresenter
import ru.gunferzs.rssreader.presentation.presenters.impl.AddRssFeedPresenter
import ru.gunferzs.rssreader.presentation.ui.base.BaseActivity

class AddRssFeedActivity : BaseActivity<IAddRssFeedPresenter.IAddRssFeedView, IAddRssFeedPresenter>(), IAddRssFeedPresenter.IAddRssFeedView {

    override fun getLayout(): Int {
        return R.layout.activity_add_rss_feed
    }

    override fun createPresenter(): IAddRssFeedPresenter {
        return AddRssFeedPresenter(this)
    }

    override fun getView(): IAddRssFeedPresenter.IAddRssFeedView {
        return this
    }

    override fun afterOnCreate(savedInstanceState: Bundle?) {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        add.setOnClickListener {
            if (!link.text.isEmpty())
                presenter.update(link.text.toString())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun showToastError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun back() {
        onBackPressed()
    }
}