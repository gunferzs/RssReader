package ru.gunferzs.rssreader.presentation.presenters.base

import ru.gunferzs.rssreader.presentation.ui.base.IView

abstract class BasePresenter<View : IView>(protected var view: View) : IPresenter