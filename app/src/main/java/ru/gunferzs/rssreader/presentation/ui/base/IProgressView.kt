package ru.gunferzs.rssreader.presentation.ui.base

interface IProgressView {
    fun showProgress()
    fun hideProgress()
}