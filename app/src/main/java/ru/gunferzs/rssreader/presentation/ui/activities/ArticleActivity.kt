package ru.gunferzs.rssreader.presentation.ui.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_article.*
import kotlinx.android.synthetic.main.main.*
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.presentation.presenters.IArticlePresenter
import ru.gunferzs.rssreader.presentation.presenters.impl.ArticlePresenter
import ru.gunferzs.rssreader.presentation.ui.base.BaseActivity

class ArticleActivity : BaseActivity<IArticlePresenter.IArticleView, IArticlePresenter>(), IArticlePresenter.IArticleView {


    override fun getLayout(): Int {
        return R.layout.activity_article
    }

    override fun createPresenter(): IArticlePresenter {
        return ArticlePresenter(this)
    }

    override fun getView(): IArticlePresenter.IArticleView {
        return this
    }

    override fun afterOnCreate(savedInstanceState: Bundle?) {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        link.setOnClickListener { presenter.clickLink() }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val id = intent.extras.getInt(MainActivity.ID_NEWS)
        presenter.update(id)
    }

    override fun getPosterView(): ImageView {
        return poster
    }

    override fun setTitle(title: String) {
        title_article.text = title
    }

    override fun setDescription(description: String) {
        this.description.text = description
    }

    override fun setDate(date: String) {
        this.date.text = date
    }

    override fun goToBrowser(link: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setData(Uri.parse(link))
        startActivity(intent)
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_bar.visibility = View.GONE
    }
}