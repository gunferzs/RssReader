package ru.gunferzs.rssreader.presentation.presenters.impl

import android.os.Bundle
import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.domain.executables.IRssFeedExecutable
import ru.gunferzs.rssreader.domain.executables.IRssFeedListExecutable
import ru.gunferzs.rssreader.domain.executables.base.IExecutable
import ru.gunferzs.rssreader.domain.executables.impl.RssFeedExecutable
import ru.gunferzs.rssreader.domain.executables.impl.RssFeedListExecutable
import ru.gunferzs.rssreader.domain.executors.impl.MainThread
import ru.gunferzs.rssreader.domain.executors.impl.ThreadExecutor
import ru.gunferzs.rssreader.presentation.presenters.IMainPresenter
import ru.gunferzs.rssreader.presentation.presenters.base.BasePresenter
import ru.gunferzs.rssreader.presentation.ui.adapters.ChannelsListAdapter
import ru.gunferzs.rssreader.presentation.ui.adapters.RssFeedAdapter
import java.util.*

class MainPresenter(view: IMainPresenter.IMainView) : BasePresenter<IMainPresenter.IMainView>(view), IMainPresenter, IRssFeedExecutable.IRssFeedResultCallback {

    companion object {
        const val TIME_DELAY = 30000L
        const val TIME_PERIOD = 30000L
    }

    var positionList = 0
    var topViewList = 0
    protected var isRunning: Boolean = false
    val mainThread = MainThread()
    lateinit var executable: IExecutable
    lateinit var executableMenu: IExecutable
    lateinit var timer: Timer

    var newsClick = object : RssFeedAdapter.ItemClick {
        override fun onClick(id: Int) {
            view.openNews(id)
        }
    }

    var menuClick = object : ChannelsListAdapter.ItemClick {
        override fun onClick(id: Int) {
            view.closeDrawer()
            clearPositionList()
            timer?.cancel()
            view.showProgress()
            updateWithId(id)
            runTimer()
        }
    }

    var listChannelsCallback = object : IRssFeedListExecutable.IRssFeedListResultCallback {
        override fun onSuccess(model: List<Rss>?) {
            model?.let {
                val adapter = ChannelsListAdapter()
                adapter.items = it
                adapter.itemClick = menuClick
                view.setMenuAdapter(adapter)
            }
        }

        override fun onError(error: String?) {

        }

        override fun onComplete() {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

    }

    override fun onStart() {

    }

    override fun onResume() {
        executableMenu = RssFeedListExecutable(listChannelsCallback, ThreadExecutor.instance, mainThread)
        executableMenu.execute()
        view.showProgress()
        update()
    }

    @Synchronized
    override fun update() {
        //view.savePositionList()
        updateWithId(RssFeedExecutable.WITHOUT_ID)
    }

    override fun updateWithId(id: Int) {
        isRunning = true
        executable = RssFeedExecutable(this, id, ThreadExecutor.instance, mainThread)
        execute()
        runTimer()
    }

    fun runTimer() {
        timer = Timer()

        timer.schedule(object : TimerTask() {

            override fun run() {
                execute()
            }
        }, TIME_DELAY, TIME_PERIOD)
    }

    fun execute() {
        executable.execute()
    }

    override fun onStop() {
        timer?.cancel()
    }

    override fun onDestroy() {

    }

    override fun onSuccess(model: Rss?) {
        model?.let {
            val adapter = RssFeedAdapter()
            it?.channel?.items?.let {
                adapter.items = it
                adapter.itemClick = newsClick
                view.setAdapter(adapter)
                view.restorePostionList(positionList, topViewList)
            }
        }
    }


    override fun onError(error: String?) {

    }

    override fun onComplete() {
        view.hideProgress()
        isRunning = false
    }

    override fun clearPositionList() {
        positionList = 0
        topViewList = 0
    }

    override fun savePositionList(position: Int, topView: Int) {
        positionList = position
        topViewList = topView
    }
}