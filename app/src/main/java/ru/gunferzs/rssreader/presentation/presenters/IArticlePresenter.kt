package ru.gunferzs.rssreader.presentation.presenters

import android.widget.ImageView
import ru.gunferzs.rssreader.presentation.presenters.base.IPresenter
import ru.gunferzs.rssreader.presentation.ui.base.IProgressView
import ru.gunferzs.rssreader.presentation.ui.base.IView

interface IArticlePresenter : IPresenter {

    fun clickLink()
    fun update(id:Int)


    interface IArticleView : IView, IProgressView {
        fun getPosterView(): ImageView
        fun setTitle(title: String)
        fun setDescription(description: String)
        fun setDate(date: String)
        fun goToBrowser(link:String)
    }
}