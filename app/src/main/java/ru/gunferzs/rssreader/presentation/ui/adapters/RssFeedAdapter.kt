package ru.gunferzs.rssreader.presentation.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ru.gunferzs.rssreader.GlideApp
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.data.model.Item

class RssFeedAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var items: List<Item>

    lateinit var itemClick: ItemClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_rss_item, parent, false)
        return RssHolder(view, itemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.get(position)
        val rssHolder = holder as RssHolder
        item.enclosure?.url?.let {
            GlideApp
                    .with(holder.itemView)
                    .load(it)
                    .centerCrop()
                    .placeholder(R.drawable.noimage)
                    .into(rssHolder.poster)
        } ?: rssHolder.poster.setImageResource(R.drawable.ic_noimage_placeholder)




        rssHolder.title.text = item.title
        rssHolder.description.text = item.description

    }

    inner class RssHolder(view: View, var itemClick: ItemClick) : RecyclerView.ViewHolder(view) {

        var poster: ImageView
        var title: TextView
        var description: TextView

        init {
            poster = view.findViewById(R.id.poster)
            title = view.findViewById(R.id.title_article)
            description = view.findViewById(R.id.description)
            view.setOnClickListener { itemClick.onClick(items[adapterPosition].id) }
        }
    }

    interface ItemClick {
        fun onClick(id: Int)
    }
}