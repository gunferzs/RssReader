package ru.gunferzs.rssreader.presentation.ui.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ru.gunferzs.rssreader.GlideApp
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.data.model.Rss

class ChannelsListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var items: List<Rss>

    lateinit var itemClick: ItemClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_channel_item, parent, false)
        return ChannelHolder(view, itemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.get(position)
        val channel = holder as ChannelHolder
        val view = channel.channel_title

        if (item.isSelected) {
            view.setTextColor(Color.BLACK)
        } else {
            view.setTextColor(Color.parseColor("#808080"))
        }

        item.channel.image?.url?.let {
            GlideApp
                    .with(holder.itemView)
                    .load(it)
                    .centerInside()
                    .placeholder(R.drawable.noimage)
                    .into(channel.poster)
        } ?: channel.poster.setImageResource(R.drawable.ic_noimage_placeholder)

        view.text = item.channel.title
    }

    fun changeSelection(id: Int) {
        for (item in items) {
            if (item.idRssFeed == id) {
                item.isSelected = true
            } else {
                item.isSelected = false
            }
        }

        notifyDataSetChanged()
    }

    inner class ChannelHolder(view: View, var itemClick: ItemClick) : RecyclerView.ViewHolder(view) {

        var channel_title: TextView
        var poster: ImageView

        init {
            channel_title = view.findViewById(R.id.channel_title)
            poster = view.findViewById(R.id.poster)
            view.setOnClickListener {
                val id = items[adapterPosition].idRssFeed
                itemClick.onClick(id)
                changeSelection(id)
            }
        }
    }

    interface ItemClick {
        fun onClick(id: Int)
    }
}