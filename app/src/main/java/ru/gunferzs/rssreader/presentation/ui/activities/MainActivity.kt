package ru.gunferzs.rssreader.presentation.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.main.*
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.presentation.presenters.IMainPresenter
import ru.gunferzs.rssreader.presentation.presenters.impl.MainPresenter
import ru.gunferzs.rssreader.presentation.ui.adapters.HorizontalDividerDecorator
import ru.gunferzs.rssreader.presentation.ui.base.BaseActivity

class MainActivity : BaseActivity<IMainPresenter.IMainView, IMainPresenter>(), IMainPresenter.IMainView, NavigationView.OnNavigationItemSelectedListener {

    lateinit var layoutManager: LinearLayoutManager

    companion object {
        const val ID_NEWS = "id_news"
    }

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun createPresenter(): IMainPresenter {
        return MainPresenter(this)
    }

    override fun getView(): IMainPresenter.IMainView {
        return this
    }

    override fun afterOnCreate(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            val intent = Intent(this, AddRssFeedActivity::class.java)
            startActivity(intent)
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        layoutManager = LinearLayoutManager(this)
        rss_feed.layoutManager = layoutManager
        rss_feed.addItemDecoration(HorizontalDividerDecorator(this))
        rss_feed.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        })
        menu_list.layoutManager = LinearLayoutManager(this)
        menu_list.addItemDecoration(HorizontalDividerDecorator(this))
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }


    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun hideProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        rss_feed.adapter = adapter
    }

    override fun savePositionList() {
        val startView = rss_feed.getChildAt(0)
        val topView = if (startView == null) 0 else (startView.top - rss_feed.paddingTop)
        presenter.savePositionList(layoutManager.findFirstVisibleItemPosition(), topView)
    }

    override fun restorePostionList(position: Int, topView: Int) {
        if (position != -1) {
            layoutManager.scrollToPositionWithOffset(position, topView)
        }
    }

    override fun openNews(id: Int) {
        val intent = Intent(this, ArticleActivity::class.java)
        intent.putExtra(ID_NEWS, id)
        startActivity(intent)
    }

    override fun setMenuAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        menu_list.adapter = adapter
    }

    override fun closeDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }
}
