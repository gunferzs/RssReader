package ru.gunferzs.rssreader.presentation.presenters.impl

import android.os.Bundle
import ru.gunferzs.rssreader.domain.executables.IAddRssFeedExecutable
import ru.gunferzs.rssreader.domain.executables.base.IExecutable
import ru.gunferzs.rssreader.domain.executables.impl.AddRssFeedExecutable
import ru.gunferzs.rssreader.domain.executors.impl.MainThread
import ru.gunferzs.rssreader.domain.executors.impl.ThreadExecutor
import ru.gunferzs.rssreader.presentation.presenters.IAddRssFeedPresenter
import ru.gunferzs.rssreader.presentation.presenters.base.BasePresenter

class AddRssFeedPresenter(view: IAddRssFeedPresenter.IAddRssFeedView) : BasePresenter<IAddRssFeedPresenter.IAddRssFeedView>(view), IAddRssFeedPresenter, IAddRssFeedExecutable.IAddRssFeedResultCallback {

    val mainThread = MainThread()
    lateinit var executable: IExecutable

    override fun onCreate(savedInstanceState: Bundle?) {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {

    }


    override fun update(link: String) {
        view.showProgress()
        executable = AddRssFeedExecutable(this, link, ThreadExecutor.instance, mainThread)
        executable.execute()
    }


    override fun onSuccess(model: Boolean?) {
        view.hideProgress()
        view.back()
    }

    override fun onError(error: String?) {
        view.showToastError(error.orEmpty())
    }

    override fun onComplete() {

    }
}