package ru.gunferzs.rssreader.presentation.presenters.impl

import android.os.Bundle
import ru.gunferzs.rssreader.GlideApp
import ru.gunferzs.rssreader.R
import ru.gunferzs.rssreader.data.model.Item
import ru.gunferzs.rssreader.domain.executables.IRssNewsItemExecutable
import ru.gunferzs.rssreader.domain.executables.impl.RssNewsItemExecutable
import ru.gunferzs.rssreader.domain.executors.impl.MainThread
import ru.gunferzs.rssreader.domain.executors.impl.ThreadExecutor
import ru.gunferzs.rssreader.presentation.presenters.IArticlePresenter
import ru.gunferzs.rssreader.presentation.presenters.base.BasePresenter

class ArticlePresenter(view: IArticlePresenter.IArticleView) : BasePresenter<IArticlePresenter.IArticleView>(view), IArticlePresenter, IRssNewsItemExecutable.IRssNewsItemResultCallback {

    lateinit var link: String
    val mainThread = MainThread()
    lateinit var executable: RssNewsItemExecutable

    override fun onCreate(savedInstanceState: Bundle?) {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {

    }

    override fun clickLink() {
        view.goToBrowser(link)
    }

    override fun update(id: Int) {
        view.showProgress()
        executable = RssNewsItemExecutable(this, id, ThreadExecutor.instance, mainThread)
        executable.execute()
    }

    override fun onSuccess(model: Item?) {
        model?.let {
            view.setTitle(it.title)
            view.setDescription(it.description)
            view.setDate(it.pubDate)
            link = it.link
            GlideApp
                    .with(view.getPosterView())
                    .load(it.enclosure.url)
                    .centerCrop()
                    .placeholder(R.drawable.noimage)
                    .into(view.getPosterView())
        }

    }

    override fun onError(error: String?) {

    }

    override fun onComplete() {
        view.hideProgress()
    }
}