package ru.gunferzs.rssreader.presentation.presenters

import android.support.v7.widget.RecyclerView
import ru.gunferzs.rssreader.presentation.presenters.base.IPresenter
import ru.gunferzs.rssreader.presentation.ui.base.IProgressView
import ru.gunferzs.rssreader.presentation.ui.base.IView

interface IMainPresenter : IPresenter {

    fun update()
    fun updateWithId(id: Int)
    fun clearPositionList()
    fun savePositionList(position: Int, topView: Int)

    interface IMainView : IView, IProgressView {
        fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>)
        fun setMenuAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>)
        fun savePositionList()
        fun restorePostionList(position: Int, topView: Int)
        fun openNews(id: Int)
        fun closeDrawer()
    }
}