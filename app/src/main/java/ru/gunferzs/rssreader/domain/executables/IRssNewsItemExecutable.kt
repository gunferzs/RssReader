package ru.gunferzs.rssreader.domain.executables

import ru.gunferzs.rssreader.data.IResult
import ru.gunferzs.rssreader.data.model.Item

interface IRssNewsItemExecutable {

    interface IRssNewsItemResultCallback : IResult<Item, String>
}