package ru.gunferzs.rssreader.domain.executables.base

interface IExecutable {
    fun execute()
}