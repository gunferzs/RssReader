package ru.gunferzs.rssreader.domain.executables

import ru.gunferzs.rssreader.data.IResult
import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.domain.executors.base.IExecutor

interface IRssFeedExecutable : IExecutor {

    interface IRssFeedResultCallback : IResult<Rss, String>
}