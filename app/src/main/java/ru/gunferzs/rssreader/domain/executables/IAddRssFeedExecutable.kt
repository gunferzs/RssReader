package ru.gunferzs.rssreader.domain.executables

import ru.gunferzs.rssreader.data.IResult

interface IAddRssFeedExecutable {

    interface IAddRssFeedResultCallback : IResult<Boolean, String>
}