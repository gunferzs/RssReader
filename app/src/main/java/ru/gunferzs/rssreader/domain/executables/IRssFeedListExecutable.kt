package ru.gunferzs.rssreader.domain.executables

import ru.gunferzs.rssreader.data.IResult
import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.domain.executors.base.IExecutor

interface IRssFeedListExecutable : IExecutor {

    interface IRssFeedListResultCallback : IResult<List<Rss>, String>
}