package ru.gunferzs.rssreader.domain.executors.impl

import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable
import ru.gunferzs.rssreader.domain.executors.base.IExecutor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class ThreadExecutor : IExecutor {

    var threadPoolExecutor: ThreadPoolExecutor

    init {
        threadPoolExecutor = ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TIME_UNIT, WORK_QUEUE)
    }

    companion object {
        const val CORE_POOL_SIZE = 3
        const val MAXIMUM_POOL_SIZE = 5
        const val KEEP_ALIVE_TIME = 120L
        val TIME_UNIT = TimeUnit.SECONDS
        val WORK_QUEUE = LinkedBlockingQueue<Runnable>()

        val instance: IExecutor by lazy {
            Holder.INSTANCE
        }
    }


    private object Holder {
        val INSTANCE = ThreadExecutor()
    }


    override fun execute(executable: BaseExecutable) {
        threadPoolExecutor.submit({
            executable.run()
            executable.onFinished()
        })
    }
}