package ru.gunferzs.rssreader.domain.executors.base

interface IMainThread {
    fun post(runnable: Runnable)
}