package ru.gunferzs.rssreader.domain.executables.impl

import ru.gunferzs.rssreader.data.model.Item
import ru.gunferzs.rssreader.data.repository.IRssNewsItemRepository
import ru.gunferzs.rssreader.data.repository.impl.RssNewsItemRepository
import ru.gunferzs.rssreader.domain.executables.IRssNewsItemExecutable
import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable
import ru.gunferzs.rssreader.domain.executors.base.IExecutor
import ru.gunferzs.rssreader.domain.executors.base.IMainThread

class RssNewsItemExecutable(var callbackPresenter: IRssNewsItemExecutable.IRssNewsItemResultCallback, var id: Int, threadExecutor: IExecutor, mainThread: IMainThread) : BaseExecutable(threadExecutor, mainThread), IRssNewsItemRepository.RssNewsResult {


    var repository: IRssNewsItemRepository

    init {
        repository = RssNewsItemRepository(this)
    }

    override fun onSuccess(model: Item?) {
        mainThread.post(Runnable { callbackPresenter.onSuccess(model) })
    }

    override fun onError(error: String?) {

    }

    override fun onComplete() {
        mainThread.post(Runnable { callbackPresenter.onComplete() })
    }

    override fun run() {
        try {
            repository.getNewsItem(id)
        } catch (ex: Exception) {
            onError(ex.message)
            onComplete()
        }
    }
}