package ru.gunferzs.rssreader.domain.executables.impl

import ru.gunferzs.rssreader.data.repository.IRssFeedRepository
import ru.gunferzs.rssreader.data.repository.impl.RssFeedRepository
import ru.gunferzs.rssreader.domain.executables.IAddRssFeedExecutable
import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable
import ru.gunferzs.rssreader.domain.executors.base.IExecutor
import ru.gunferzs.rssreader.domain.executors.base.IMainThread

class AddRssFeedExecutable(var callbackPresenter: IAddRssFeedExecutable.IAddRssFeedResultCallback, var link: String, threadExecutor: IExecutor, mainThread: IMainThread) : BaseExecutable(threadExecutor, mainThread), IRssFeedRepository.RssFeedResult {

    var repository: IRssFeedRepository

    init {
        repository = RssFeedRepository(this)
    }


    override fun run() {
        try {
            repository.addLink(link)
        } catch (ex: Exception) {
            onError(ex.message)
            onComplete()
        }
    }

    override fun onSuccess(model: Boolean?) {
        mainThread.post(Runnable { callbackPresenter.onSuccess(model) })
    }

    override fun onError(error: String?) {
        mainThread.post(Runnable { callbackPresenter.onError(error) })
    }

    override fun onComplete() {
        mainThread.post(Runnable { callbackPresenter.onComplete() })
    }
}