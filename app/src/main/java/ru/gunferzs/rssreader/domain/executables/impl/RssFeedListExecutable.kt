package ru.gunferzs.rssreader.domain.executables.impl

import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.data.repository.IRssRepository
import ru.gunferzs.rssreader.data.repository.impl.RssRepository
import ru.gunferzs.rssreader.domain.executables.IRssFeedListExecutable
import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable
import ru.gunferzs.rssreader.domain.executors.base.IExecutor
import ru.gunferzs.rssreader.domain.executors.base.IMainThread

class RssFeedListExecutable(var callbackPresenter: IRssFeedListExecutable.IRssFeedListResultCallback, threadExecutor: IExecutor, mainThread: IMainThread) : BaseExecutable(threadExecutor, mainThread), IRssRepository.ChannelsResult {

    var repository: IRssRepository

    init {
        repository = RssRepository()
        (repository as RssRepository).callbackListChannels = this
    }

    override fun run() {
        try {
            repository.getListChannels()
        } catch (ex: Exception) {
            onError(ex.message)
            onComplete()
        }

    }

    override fun onSuccess(model: List<Rss>?) {
        mainThread.post(Runnable { callbackPresenter.onSuccess(model) })
    }

    override fun onError(error: String?) {
        mainThread.post(Runnable { callbackPresenter.onError(error) })
    }

    override fun onComplete() {
        mainThread.post(Runnable { callbackPresenter.onComplete() })
    }
}