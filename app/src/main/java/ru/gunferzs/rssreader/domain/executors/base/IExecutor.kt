package ru.gunferzs.rssreader.domain.executors.base

import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable

interface IExecutor {
    fun execute(executable: BaseExecutable)
}