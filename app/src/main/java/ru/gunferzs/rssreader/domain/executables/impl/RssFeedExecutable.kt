package ru.gunferzs.rssreader.domain.executables.impl

import ru.gunferzs.rssreader.data.model.Rss
import ru.gunferzs.rssreader.data.repository.IRssRepository
import ru.gunferzs.rssreader.data.repository.impl.RssRepository
import ru.gunferzs.rssreader.domain.executables.IRssFeedExecutable
import ru.gunferzs.rssreader.domain.executables.base.BaseExecutable
import ru.gunferzs.rssreader.domain.executors.base.IExecutor
import ru.gunferzs.rssreader.domain.executors.base.IMainThread

class RssFeedExecutable(var callbackPresenter: IRssFeedExecutable.IRssFeedResultCallback, var id: Int, threadExecutor: IExecutor, mainThread: IMainThread) : BaseExecutable(threadExecutor, mainThread), IRssRepository.RssResult {

    var repository: IRssRepository

    init {
        repository = RssRepository()
        (repository as RssRepository).callbackRss = this
    }

    companion object {
        const val WITHOUT_ID = -1;
    }

    override fun onSuccess(model: Rss?) {
        mainThread.post(Runnable { callbackPresenter.onSuccess(model) })
    }

    override fun onComplete() {
        mainThread.post(Runnable { callbackPresenter.onComplete() })
    }

    override fun onError(error: String?) {
        mainThread.post(Runnable { callbackPresenter.onError(error) })
    }

    override fun run() {
        try {
            if (id.equals(WITHOUT_ID)) {
                repository.downloadRssNewsWithSavedId()
            } else {
                repository.downloadRssNews(id)
            }
        } catch (ex: Exception) {
            onError(ex.message)
            onComplete()
        }
    }
}